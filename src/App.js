import React, { useState, useRef } from 'react';
import { Navbar, Nav, FormControl, Button, Container, InputGroup, Row, Col } from 'react-bootstrap';
import SearchResults from './components/SearchResults';
import AutoComplete from './components/AutoComplete';
import './App.css';


const Main = () => {
  const [search, setSearch] = useState("");
  const [autocomplete, setAutocomplete] = useState("");
  const [focused, setFocused] = useState(false);
  const searchEl = useRef(null);

  const handleSearch = event => {
    setAutocomplete(event.target.value);
    setFocused(true);
  }

  const onResultClick = value => {
    setAutocomplete(value);
    setSearch(value);
    setFocused(false);
  }

  const onKeyPress = event => {
    if(event.which === 13) {
      setSearch(searchEl.current.value);
      setFocused(false);
    }
  }

  return (
    <div className="App" onKeyPress={onKeyPress}>
      <header>
        <Navbar expand="lg">
          <Navbar.Brand href="#home">Property Directory</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
              <Nav>
                <Nav.Link href="#"><b>CREATE</b></Nav.Link>
                <Nav.Link href="#"><b>UPDATE</b></Nav.Link>
                <Nav.Link href="#"><b>DELETE</b></Nav.Link>
              </Nav>
          </Navbar.Collapse>
          </Navbar>
      </header>
      <Container className="p-5 mt-5">
        <Row className="mb-5">
          <Col>
            <h1>
              <span className="jam jam-building"></span>Property Directory Search
            </h1>
            <br/>
            <h6>by: Juan Alphonso D. Maligad</h6>
          </Col>
        </Row>
        <Row className="mb-5">
          <Col>
          <InputGroup>
            <FormControl className="shadow p-4" size="lg" type="text" placeholder="Search" onClick={handleSearch} onChange={handleSearch} ref={searchEl} value={autocomplete} />
            <InputGroup.Append>
              <Button className="shadow" variant="outline-success" onClick={() => setSearch(searchEl.current.value)}>Search</Button>
            </InputGroup.Append>
          </InputGroup>
          {focused ? <AutoComplete props={autocomplete} onResultClick={onResultClick}/> : ''}
          </Col>
        </Row>
        <Row className="pt-2">
          <Col>
            <SearchResults props={search}/>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Main
