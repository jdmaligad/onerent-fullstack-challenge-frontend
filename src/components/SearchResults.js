import React from 'react';
import { Container, Row, Col, Card, Badge, Alert } from 'react-bootstrap';
import { useQuery, gql } from '@apollo/client';


const SEARCH_QUERY = gql`
    query Search($value: String) {
        search(value: $value) {
            id
            street
            city
            state
            zip
            rent
            User {
                id
                firstName
                lastName
            }
        }
    }
`;

const SearchResults = props => {
    const { loading, error, data } = useQuery(SEARCH_QUERY, {variables: { value: props.props }});

    if (loading) return 'Loading ...';
    if (error) return `Error! ${error.message}`;

    return (
      <Container className="text-left px-0" fluid>
        {data.search.length > 0 ? 
            data.search.map(result => (
                <Row key={result.id} className="mb-2">
                    <Col>
                        <Card className="shadow">
                            <Card.Body>
                                <Card.Title>{result.street}, {result.city}, {result.state}, {result.zip}</Card.Title>
                                <Card.Subtitle className="mb-2 text-info">Rent: ${result.rent}/month</Card.Subtitle>
                                <Card.Text>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a nulla et magna consectetur consectetur at eget ligula. Vestibulum tempus risus id nibh auctor, eu tristique ex sodales.
                                </Card.Text>
                            {(result.User) ? <Badge pill variant="secondary">Rented</Badge> : <Badge pill variant="success">Vacant</Badge>}
                            </Card.Body>
                            {(result.User) ? <Card.Footer className="text-muted">Occupied by: {result.User.firstName} {result.User.lastName}</Card.Footer> : ''}
                        </Card>
                    </Col>
                </Row>
              ))
            :
            <Alert variant="warning">
                <Alert.Heading><span className="jam jam-triangle-danger-f"></span> No records found</Alert.Heading>
                <p>
                    Either the database records are empty or there are no results that returned from your input. Please try again.
                </p>
            </Alert>
        }
      </Container>
    );
  }

export default SearchResults