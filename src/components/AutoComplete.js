import React from 'react';
import { ListGroup } from 'react-bootstrap';
import { useQuery, gql } from '@apollo/client';


const SEARCH_QUERY = gql`
    query Search($value: String) {
        search(value: $value) {
            id
            street
            city
            state
        }
    }
`;

const AutoComplete = (props) => {
    const { loading, error, data } = useQuery(SEARCH_QUERY, {variables: { value: props.props }});

    if (loading) return '';
    if (error) return `Error! ${error.message}`;

    var distinct = [];

    data.search.map(result => {
        if (!distinct.includes(result.city.toString() + ", " + result.state.toString())) {
            distinct.push(result.city.toString() + ", " + result.state.toString())
        }
        return 1
    })
    return (
        <ListGroup className="shadow text-left bg-white rounded border position-absolute d-block" style={{ zIndex: 10 }}>
            { distinct.map(result => (
                <ListGroup.Item action key={result} onClick={() => props.onResultClick(result.toString())}>
                    <span className="jam jam-gps pr-3"></span>{result}
                </ListGroup.Item>
            ))}
        </ListGroup>
    );
}

export default AutoComplete